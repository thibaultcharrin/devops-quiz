# Python - intermediate

### [README](./README.md)

## Discuss the differences between Python 2 and Python 3, including key language features and compatibility considerations. How do you handle migration from Python 2 to Python 3 in existing codebases?

- Key Language Features:

    - Unicode: Python 3 treats strings as Unicode by default, whereas Python 2 used ASCII.
    - Print Function: In Python 3, print is a function (print()), while in Python 2, it's a statement (print).
    - Integer Division: In Python 3, division between integers results in a float, while in Python 2, it returns an integer.
    - Syntax: Python 3 introduced syntax changes and enhancements, such as async and await for asynchronous programming.

- Compatibility Considerations:

    - Libraries: Some libraries may not be fully compatible with both Python 2 and Python 3.
    - Ecosystem: Python 2 reached end-of-life in 2020, so Python 3 is the recommended version for new projects.
    - Migration Tools: Tools like 2to3 and futurize assist in converting Python 2 code to Python 3.

- Migration from Python 2 to Python 3:

    - Identify Dependencies: Ensure all libraries and dependencies support Python 3.
    - Use Compatibility Libraries: Libraries like six help write code compatible with both Python versions.
    - Automated Tools: Utilize automated migration tools like 2to3 for basic conversion.
    - Manual Refactoring: Manually refactor code where automated tools may not be sufficient.
    - Continuous Testing: Test thoroughly to ensure functionality is maintained during the migration process.
    - Incremental Approach: Migrate codebase incrementally, focusing on critical components first.

## Explain the concept of virtual environments in Python. How do you create and manage virtual environments using tools like virtualenv or venv?

```bash
#!/bin/bash

# Create a virtual environment directory
python3 -m venv myenv

# Activate the virtual environment
source myenv/bin/activate

# Install dependencies using pip
pip install package_name

# Deactivate the virtual environment
deactivate
```

```python
#!/usr/bin/env python3

import os
import subprocess

# Create a virtual environment directory
subprocess.run(["python3", "-m", "venv", "myenv"], check=True)

# Activate the virtual environment
activate_script = os.path.join("myenv", "bin", "activate")
subprocess.run(["source", activate_script], shell=True, check=True)

# Install dependencies using pip
subprocess.run(["pip", "install", "package_name"], check=True)

# Deactivate the virtual environment
subprocess.run(["deactivate"], shell=True, check=True)
```

- You can save either of these scripts to a file (e.g., create_virtualenv.sh or create_virtualenv.py) and execute them in your Linux environment. Make sure to replace "package_name" with the actual package you want to install.

## Describe the role of package management in Python. How do you use tools like pip and requirements.txt files to manage dependencies in Python projects?

- pip: Pip is the package installer for Python. It allows users to install and manage Python packages from the Python Package Index (PyPI) or other sources. Here's how you use it:

    ```bash
    # Install a package
    pip install package_name

    # Install a specific version of a package
    pip install package_name==version

    # Upgrade a package
    pip install --upgrade package_name

    # Uninstall a package
    pip uninstall package_name
    ```

- requirements.txt: This file is commonly used to specify project dependencies along with their versions. It simplifies dependency management and ensures consistent installations across different environments. Here's how you use it:

    ```txt
    # requirements.txt
    package1==1.0.0
    package2>=2.1.0
    ```

    ```bash
    pip install -r requirements.txt

    ```

- Using these tools and files, developers can easily manage project dependencies, making it easier to share and reproduce their projects across different environments.

## Discuss the principles of object-oriented programming (OOP) in Python. Provide examples of class inheritance, encapsulation, and polymorphism in Python code.

- In Python, object-oriented programming (OOP) is based on the following principles:

- Class and Object: A class is a blueprint for creating objects, and an object is an instance of a class.

- Encapsulation: Encapsulation refers to the bundling of data and methods that operate on the data within a single unit, called a class. It helps in hiding the internal state and requiring all interaction to be performed through an object's methods. Here's an example:

    ```python
    class Car:
        def __init__(self, make, model):
            self.make = make
            self.model = model

        def drive(self):
            return f"{self.make} {self.model} is driving"
    ```

- Inheritance: Inheritance allows a class (subclass) to inherit attributes and methods from another class (superclass). It promotes code reusability and establishes a parent-child relationship between classes. Here's an example:

    ```python
    class ElectricCar(Car):
        def __init__(self, make, model, battery_capacity):
            super().__init__(make, model)
            self.battery_capacity = battery_capacity

        def charge(self):
            return f"{self.make} {self.model} is charging"
    ```

- Polymorphism: Polymorphism allows objects of different classes to be treated as objects of a common superclass. It enables a single interface to represent different types of objects. Here's an example:

    ```python
    class Animal:
        def sound(self):
            pass

    class Dog(Animal):
        def sound(self):
            return "Woof"

    class Cat(Animal):
        def sound(self):
            return "Meow"

    def make_sound(animal):
        return animal.sound()

    dog = Dog()
    cat = Cat()

    print(make_sound(dog))  # Output: Woof
    print(make_sound(cat))  # Output: Meow
    ```

- These principles help in writing modular, scalable, and maintainable code in Python.

## Explain the use of decorators in Python. How do decorators enhance code readability and maintainability, and what are some common use cases for decorators?

- Decorators in Python are functions that modify the behavior of other functions or methods. They allow you to wrap another function or method in order to extend or modify its behavior without modifying its code directly.

- Decorators enhance code readability and maintainability by promoting code reuse and separating concerns. They allow you to add functionality to existing functions or methods without cluttering their implementation.

- Common use cases for decorators include:

    - Logging: Adding logging statements to functions or methods to track their execution.

    - Authorization: Restricting access to certain functions or methods based on user permissions.

    - Caching: Memoizing expensive function calls to improve performance.

    - Validation: Validating input parameters before executing functions or methods.

    - Timing: Measuring the execution time of functions or methods.

- Here's an example of a simple decorator that logs the execution of a function:

    ```python
    def log_execution(func):
        def wrapper(*args, **kwargs):
            print(f"Executing function: {func.__name__}")
            return func(*args, **kwargs)
        return wrapper

    @log_execution
    def greet(name):
        return f"Hello, {name}!"

    print(greet("John"))
    ```

- In this example, the log_execution decorator wraps the greet function and adds logging functionality before and after executing the function. This allows you to log the execution of greet without modifying its implementation.

## Describe the benefits of using Python's asyncio module for asynchronous programming. How do you write asynchronous code using async/await syntax?

- The asyncio module in Python provides a framework for writing asynchronous code using the async and await syntax. Asynchronous programming allows you to write non-blocking, concurrent code that can efficiently handle I/O-bound and CPU-bound tasks without blocking the event loop.

- Some benefits of using asyncio for asynchronous programming include:

    - Improved Performance: Asynchronous code allows for more efficient utilization of system resources by avoiding blocking operations and leveraging event-driven programming.

    - Scalability: Asynchronous programming enables the handling of many concurrent connections and tasks with relatively few threads or processes, leading to better scalability.

    - Simplified Code: Using async and await syntax makes it easier to write and understand asynchronous code compared to traditional callback-based approaches.

    - Better Resource Utilization: Asynchronous code can handle I/O-bound tasks more efficiently by allowing other tasks to execute while waiting for I/O operations to complete, leading to better resource utilization.

- To write asynchronous code using async/await syntax in Python, you define asynchronous functions using the async keyword and use the await keyword to await asynchronous operations. Here's an example:

    ```python
    import asyncio

    async def greet(name):
        print(f"Hello, {name}!")
        await asyncio.sleep(1)  # Simulate an asynchronous operation (e.g., I/O)
        print(f"Goodbye, {name}!")

    async def main():
        await asyncio.gather(
            greet("Alice"),
            greet("Bob"),
            greet("Charlie")
        )

    asyncio.run(main())
    ```

- In this example, the greet function is defined as an asynchronous function using the async keyword. Inside greet, await asyncio.sleep(1) is used to simulate an asynchronous operation (e.g., waiting for I/O). The main function uses asyncio.gather to concurrently execute multiple asynchronous tasks. Finally, asyncio.run(main()) is used to run the main coroutine and execute the asynchronous code.

## Discuss the importance of testing in Python development. What are some popular testing frameworks and libraries in Python, and how do you write unit tests and integration tests for Python code?

- Testing is a crucial aspect of Python development as it helps ensure code quality, reliability, and maintainability. By writing tests, developers can verify that their code behaves as expected, detect bugs early in the development process, and prevent regressions when making changes to the codebase.

- Some popular testing frameworks and libraries in Python include:

    - unittest: Python's built-in unit testing framework, inspired by the JUnit framework for Java. It provides a basic framework for writing and executing unit tests.

    - pytest: A third-party testing framework that offers a more flexible and powerful alternative to unittest. It supports features like fixtures, parameterized testing, and powerful assertion statements.

    - nose: Another third-party testing framework that extends unittest and provides additional features like test discovery, plugins, and parallel test execution.

    - doctest: A module for running tests embedded in docstrings. It allows developers to write tests directly within function and module docstrings, making it convenient for documenting and testing code simultaneously.

- To write unit tests and integration tests for Python code using pytest, for example, you would typically follow these steps:

- Install pytest: You can install pytest using pip:

    ```bash
    pip install pytest
    ```

- Write test functions: Define test functions that verify the behavior of your code. Test functions should be named with a prefix of test_ and can use pytest's assertion statements to verify expected outcomes.

    ```python
    # test_example.py

    def add(a, b):
        return a + b

    def test_add():
        assert add(1, 2) == 3
        assert add(0, 0) == 0
        assert add(-1, 1) == 0
    ```

- Run tests: Execute the tests using the pytest command:

    ```bash
    pytest
    ```

- This will discover and run all test functions in the current directory and its subdirectories.

- Integration tests, which verify interactions between different components or modules of the system, can also be written using pytest or other testing frameworks. These tests typically involve setting up a test environment, executing code that spans multiple components, and asserting the expected outcomes.

- By incorporating testing into the development process, developers can ensure code reliability, catch bugs early, and build confidence in the correctness of their code.

## Explain the concept of generators and iterators in Python. How do you create and use generator functions and generator expressions in Python?

- In Python, iterators and generators are used to work with sequences of data efficiently, especially when dealing with large datasets or infinite sequences.

- Iterators: An iterator is an object that represents a stream of data. It implements two methods: __iter__() and __next__(). The __iter__() method returns the iterator object itself, and the __next__() method returns the next item in the sequence or raises a StopIteration exception when the sequence is exhausted.

-Generators: Generators are a special type of iterator that can be defined using generator functions or generator expressions. They allow you to generate values lazily, on-the-fly, instead of storing them in memory all at once. This makes them particularly useful for handling large or infinite sequences of data.

- Here's how you create and use generator functions and generator expressions in Python:

- Generator Functions:

    ```python
    def my_generator():
        yield 1
        yield 2
        yield 3

    # Using the generator function
    gen = my_generator()
    print(next(gen))  # Output: 1
    print(next(gen))  # Output: 2
    print(next(gen))  # Output: 3
    ```

- Generator Expressions:

    ```python
    # Generator expression to generate squares of numbers
    gen = (x ** 2 for x in range(5))

    # Using the generator expression
    for num in gen:
        print(num)
    # Output:
    # 0
    # 1
    # 4
    # 9
    # 16
    ```

- In both cases, the yield keyword is used to define points at which the function will temporarily pause and yield control back to the caller, returning a value. This allows the generator to resume execution from where it left off when the next value is requested.

- Generators are memory-efficient because they generate values one at a time, as opposed to storing the entire sequence in memory. They are especially useful for processing large datasets or infinite sequences, where storing all values in memory would be impractical or impossible.

## Describe the process of working with files and directories in Python. How do you read from and write to files, and how do you manipulate file paths using the os and pathlib modules?

- Reading from and Writing to Files:

    - To read from a file, you can use the open() function with the appropriate mode ('r' for reading, 'w' for writing, 'a' for appending, etc.).
    - To write to a file, you can open it in write mode ('w') and use methods like write() to write data to the file.
    - Example of reading from a file:

    ```python
    with open('example.txt', 'r') as f:
        data = f.read()
        print(data)
    ```

    - Example of writing to a file:

    ```python
    with open('example.txt', 'w') as f:
        f.write('Hello, world!')
    ```

- Manipulating File Paths:

    - The os.path module provides functions for manipulating file paths in a platform-independent way.
    - The pathlib module offers an object-oriented approach for working with file paths.
    - Example using os.path:

    ```python
    import os

    # Joining paths
    file_path = os.path.join('path', 'to', 'file.txt')

    # Getting the directory name and file name
    dir_name = os.path.dirname(file_path)
    file_name = os.path.basename(file_path)

    # Checking if a file or directory exists
    if os.path.exists(file_path):
        print('File exists!')
    ```

    - Example using pathlib:

    ```python
    from pathlib import Path

    # Creating a Path object
    file_path = Path('path') / 'to' / 'file.txt'

    # Getting the directory name and file name
    dir_name = file_path.parent
    file_name = file_path.name

    # Checking if a file or directory exists
    if file_path.exists():
        print('File exists!')
    ```

- Both os.path and pathlib modules offer robust functionality for working with file paths, allowing you to perform various operations such as joining paths, checking file existence, and extracting directory and file names. Choose the one that suits your coding style and requirements.

## Discuss the role of Python in automation and scripting tasks for DevOps. Provide examples of how Python is used for tasks such as configuration management, deployment automation, and monitoring automation.

- Python plays a crucial role in automation and scripting tasks for DevOps due to its versatility, ease of use, and extensive libraries and frameworks. Here are some examples of how Python is used in various DevOps tasks:

- Configuration Management:

    - Infrastructure as Code (IaC): Python scripts are used to define and manage infrastructure resources using tools like Terraform or AWS CloudFormation.
    - Configuration Templating: Python's Jinja2 library is commonly used to generate configuration files dynamically based on templates and variables.
    - Custom Configuration Management Tools: Python scripts can be developed to automate configuration management tasks specific to the organization's requirements.

- Deployment Automation:

    - Continuous Integration/Continuous Deployment (CI/CD): Python scripts are used in CI/CD pipelines to automate building, testing, and deploying applications.
    - Deployment Orchestration: Tools like Ansible, Fabric, or custom Python scripts automate deployment processes across multiple servers or environments.

- Monitoring Automation:

    - Monitoring Scripting: Python scripts integrate with monitoring tools like Prometheus, Nagios, or Zabbix to automate monitoring tasks, collect metrics, and generate alerts.
    - Log Parsing and Analysis: Python scripts process log files to extract meaningful information, detect anomalies, and trigger automated responses.

- Task Automation:

    - Routine Maintenance Tasks: Python scripts automate routine tasks such as backups, log rotation, and database maintenance.
    - Scheduled Jobs: Python's cron or schedule libraries schedule recurring tasks such as data synchronization, cleanup, or report generation.

- Infrastructure Provisioning:

    - Cloud Automation: Python scripts interact with cloud provider APIs (e.g., AWS Boto3, Azure SDK) to provision, manage, and scale cloud resources.
    - Container Orchestration: Python-based tools like Kubernetes or Docker Compose automate container deployment, scaling, and management.

- Overall, Python's flexibility, extensive ecosystem of libraries, and easy integration with other tools make it a preferred choice for automating various DevOps tasks, enabling faster and more efficient development, deployment, and maintenance of software systems.

### [<- Previous](./python-basic.md) [Next ->](./storage-basic.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)