# Kubernetes - basic

### [README](./README.md)

## What is Kubernetes, and what problem does it solve in the context of managing containerized applications?

- Kubernetes is an open-source container orchestration platform that automates the deployment, scaling, and management of containerized applications. It solves the problem of deploying and managing applications at scale in distributed environments by providing features like automatic scaling, self-healing, service discovery, and load balancing.

## Explain the difference between a Kubernetes Pod and a Kubernetes Deployment.

- A Kubernetes Pod is the smallest deployable unit in Kubernetes, representing one or more containers that share network and storage resources. A Deployment, on the other hand, manages the lifecycle and scaling of multiple replicas of Pods. It ensures that a specified number of identical Pods are running and provides features like rolling updates and rollbacks for application deployments.

## How do you scale a Kubernetes Deployment horizontally to handle increased traffic?

- To scale a Kubernetes Deployment horizontally, you can use the kubectl scale command or update the Deployment's YAML file directly. You specify the desired number of replicas, and Kubernetes automatically adjusts the number of Pods to match the desired state. For example, to scale a Deployment named "app-deployment" to five replicas, you would run:

```bash
kubectl scale deployment app-deployment --replicas=5
```

## What is a Kubernetes Service, and why is it necessary for communication between Pods?

- A Kubernetes Service is an abstraction layer that provides a consistent way to access a set of Pods in Kubernetes. It acts as a permanent IP address and DNS name for a logical set of Pods, allowing other Pods to communicate with them regardless of their underlying network topology. Services enable load balancing, service discovery, and routing traffic to Pods based on labels and selectors, making it necessary for seamless communication between Pods within a Kubernetes cluster.

## How do you expose a Kubernetes Deployment externally so that it can be accessed from the internet?

- To expose a Kubernetes Deployment externally, you can create a Kubernetes Service of type LoadBalancer or NodePort.

    - LoadBalancer: This type of Service provisions an external load balancer in the cloud provider's environment, which routes traffic to the Kubernetes Service. The load balancer typically has a public IP address accessible from the internet.

    - NodePort: This type exposes the Service on a static port on each Node of the Kubernetes cluster. You can access the Service using any Node's IP address and the specified static port.

- Once the Service is created, it will be accessible from the internet through the assigned IP address and port.

## Describe the purpose of a Kubernetes ConfigMap and how it is used to manage application configuration.

- A Kubernetes ConfigMap is used to decouple configuration data from containerized applications. It allows you to store configuration settings, such as environment variables, file paths, or key-value pairs, separately from the application code. ConfigMaps can be mounted as volumes or injected into Pods as environment variables, enabling dynamic configuration updates without redeploying the application. This separation of configuration data from application logic enhances portability, scalability, and maintainability of containerized applications in Kubernetes.

## What is a Kubernetes Namespace, and why would you use it in a Kubernetes cluster?

- A Kubernetes Namespace is a logical partition within a Kubernetes cluster that enables multiple virtual clusters to coexist within the same physical cluster. It provides a scope for Kubernetes resources, such as Pods, Services, Deployments, and ConfigMaps, allowing teams or users to segregate and isolate their workloads, environments, or projects. Namespaces help in organizing, managing, and securing resources within a Kubernetes cluster by providing a way to enforce resource quotas, access controls, and resource naming conventions. They are commonly used to support multi-tenancy, separate development, staging, and production environments, and streamline cluster administration and resource management.

## Explain the concept of a Kubernetes Node and its role in a Kubernetes cluster.

- A Kubernetes Node is a physical or virtual machine that serves as a worker in a Kubernetes cluster. Nodes are responsible for running containers, scheduling workloads, and providing necessary services to manage and maintain the cluster's operation. Each Node typically runs a container runtime, such as Docker or containerd, along with other essential Kubernetes components, including the kubelet, kube-proxy, and container runtime interface (CRI) plugin. Nodes communicate with the control plane, consisting of the Kubernetes Master components, to receive instructions, report their status, and participate in cluster-wide orchestration tasks. Nodes play a crucial role in distributing and executing workloads across the cluster, providing scalability, resilience, and resource utilization optimization.

## How do you update a Kubernetes Deployment to use a new version of your container image?

- To update a Kubernetes Deployment with a new version of a container image, you typically perform the following steps:

    - Update the Container Image: Push the new version of your container image to a container registry, such as Docker Hub or Google Container Registry.

    - Update the Deployment Manifest: Modify the Deployment YAML file to specify the new version of the container image. You can change the image tag or digest to reference the new version.

    - Apply the Changes: Use the kubectl apply command to apply the updated Deployment manifest to the Kubernetes cluster. This will trigger the Deployment controller to reconcile the changes and update the Pods running the application.

    - Rolling Update: Kubernetes automatically performs a rolling update by gradually replacing old Pods with new ones, ensuring zero-downtime deployment. You can monitor the rollout status using kubectl rollout status or kubectl get pods.

    - Verify the Update: Once the update is complete, verify that the new version of the application is running as expected by testing its functionality and monitoring the Pods' status.

## Describe the Kubernetes Control Plane components and their functions in managing the cluster.

- The Kubernetes Control Plane consists of several components that manage the cluster's state and coordinate its operations:

    - API Server: The Kubernetes API server exposes the Kubernetes API, which acts as the front end for the Kubernetes control plane. It handles requests from users, controllers, and other components, validating and processing them against the cluster state stored in the etcd database.

    - Scheduler: The Kubernetes Scheduler is responsible for placing Pods onto available Nodes in the cluster. It considers factors like resource requirements, quality of service requirements, affinity and anti-affinity specifications, and data locality when making scheduling decisions.

    - Controller Manager: The Controller Manager runs controller processes that regulate the state of the cluster, ensuring that the actual state matches the desired state. Controllers include the Node Controller, Replication Controller, Endpoints Controller, Service Account & Token Controllers, and more.

    - etcd: etcd is a distributed key-value store used to store the cluster's state. It serves as the single source of truth for the entire Kubernetes cluster, providing reliable storage for configuration data and the current state of all Kubernetes objects.

    - kube-scheduler: The kube-scheduler component is responsible for selecting a Node for newly created Pods. It evaluates various factors such as resource requirements, affinity/anti-affinity rules, and workload constraints to make informed scheduling decisions.

- These components work together to ensure the proper functioning of the Kubernetes cluster, handling API requests, scheduling workloads, maintaining desired state, and storing cluster data reliably.

### [<- Previous](./jenkins-intermediate.md) [Next ->](./k8s-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)