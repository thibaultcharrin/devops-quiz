# GCP - basic

### [README](./README.md)

## What is Google Cloud Platform (GCP), and what are its main services?

- Compute: GCP offers several compute services for running applications and workloads in the cloud, including:

    - Google Compute Engine (GCE): Infrastructure as a Service (IaaS) offering for virtual machines (VMs) running on Google's global infrastructure.
    - Google Kubernetes Engine (GKE): Managed Kubernetes service for orchestrating containerized applications.
    - Google App Engine: Platform as a Service (PaaS) offering for building and deploying scalable web applications and APIs.

- Storage: GCP provides various storage services for storing and managing data in the cloud, such as:

    - Google Cloud Storage: Object storage service for storing and retrieving unstructured data, including files, images, and videos.
    - Google Cloud SQL: Managed relational database service based on MySQL, PostgreSQL, and SQL Server.
    - Google Cloud Bigtable: Fully managed NoSQL database service for large-scale, low-latency workloads.

- Networking: GCP offers networking services to connect and manage resources in the cloud, including:

    - Virtual Private Cloud (VPC): Networking service for creating and managing virtual networks, subnets, and firewall rules.
    - Cloud Load Balancing: Fully managed, scalable load balancing service for distributing incoming traffic across multiple instances or regions.
    - Cloud DNS: Managed Domain Name System (DNS) service for hosting and managing domain names and DNS records.

- Big Data and Machine Learning: GCP provides a suite of services for analyzing and processing large datasets and building machine learning models, such as:

    - Google BigQuery: Fully managed, serverless data warehouse for analyzing large-scale datasets using SQL queries.
    - Google Cloud Dataflow: Managed stream and batch processing service for building data pipelines.
    - Google Cloud AI Platform: Unified platform for building, training, and deploying machine learning models at scale.

- Developer Tools: GCP offers developer tools and services to facilitate application development and deployment, including:

    - Google Cloud Build: Fully managed continuous integration and continuous delivery (CI/CD) service for automating build and deployment pipelines.
    - Google Cloud Functions: Serverless compute service for running event-driven functions in response to cloud events.
    - Google Cloud SDK: Command-line tools and libraries for managing GCP resources and interacting with GCP services.

- Management and Monitoring: GCP provides management and monitoring tools to monitor, optimize, and secure cloud resources, such as:

    - Google Cloud Console: Web-based user interface for managing and monitoring GCP resources and services.
    - Google Cloud Monitoring: Monitoring and observability service for collecting, analyzing, and visualizing metrics, logs, and traces.
    - Google Cloud Identity and Access Management (IAM): Identity and access management service for controlling access to GCP resources and services.

## How do you create a virtual machine instance in Google Compute Engine?

- Navigate to the Google Cloud Console.
- Go to the Compute Engine section.
- Click on "Create Instance."
- Provide details such as instance name, machine type, boot disk type, and image.
- Configure additional settings like machine type, disk size, and networking options if needed.
- Click "Create" to provision the virtual machine instance.

## What is Google Cloud Storage, and how is it used for storing data?

- Google Cloud Storage is a scalable and durable object storage service provided by Google Cloud Platform (GCP). It allows users to store and retrieve data in the cloud in a highly available and secure manner. Data stored in Google Cloud Storage is organized into buckets, which are containers for objects. Each object consists of data, metadata, and a unique identifier.

- Google Cloud Storage is used for storing various types of data, including:

    - Unstructured data: Such as images, videos, audio files, documents, backups, and log files.
    - Static website content: Hosting static websites by storing HTML, CSS, JavaScript, and other web assets.
    - Data for analytics and machine learning: Storing large datasets for analysis, training machine learning models, and running data processing pipelines.
    - Backup and disaster recovery: Storing backup copies of critical data and applications for disaster recovery purposes.
    - Content distribution: Distributing large files or media content to users worldwide through a global network of edge locations using Google Cloud CDN (Content Delivery Network).

- Users can interact with Google Cloud Storage using various methods, including the Google Cloud Console, command-line interface (CLI), and client libraries for programming languages such as Python, Java, and Node.js. They can upload, download, list, and manage objects in buckets, as well as set access controls, encryption, and lifecycle policies to manage the storage of data efficiently and securely.

## Explain the concept of regions and zones in GCP.

- In Google Cloud Platform (GCP), regions are geographical locations where Google data centers are located. Each region is a separate geographic area and consists of one or more zones. Zones are isolated locations within regions that are engineered to be independent of each other in terms of power, cooling, and networking.

- The concept of regions and zones allows users to deploy resources and distribute workloads across multiple geographic locations for redundancy, high availability, and fault tolerance. By strategically placing resources in different regions and zones, users can minimize the impact of outages, disasters, and network latency, and ensure reliable performance for their applications and services.

- For example, a user might deploy virtual machine instances in multiple zones within the same region to achieve redundancy and fault tolerance. Alternatively, they might distribute their resources across multiple regions to ensure geographic diversity and mitigate the risk of regional failures.

- Overall, regions and zones in GCP provide users with the flexibility to design and deploy their infrastructure in a way that meets their specific requirements for availability, performance, and resilience.

## How do you deploy a web application to Google App Engine?

- Prepare your application: Ensure your web application follows the App Engine specifications, including using supported programming languages (e.g., Python, Java, Node.js), and adheres to the App Engine runtime environment requirements.

- Create an App Engine project: Use the Google Cloud Console to create a new App Engine project or select an existing one.

- Configure your application: Update your application's configuration files, such as app.yaml for App Engine flexible environment or appengine-web.xml for App Engine standard environment, to specify runtime settings, scaling options, and other configurations.

- Package your application: Package your web application and its dependencies into a deployable format, such as a JAR file for Java applications or a Docker image for custom runtimes in the flexible environment.

- Deploy your application: Use the Google Cloud SDK or Cloud Console to deploy your application to App Engine. Run the gcloud app deploy command from the root directory of your application to upload and deploy your application to the specified App Engine environment (standard or flexible).

- Monitor deployment: Monitor the deployment process in the Google Cloud Console or using the Cloud SDK commands to track the progress and status of the deployment. Once the deployment is complete, your web application will be accessible via the provided App Engine URL.

## What is Google Kubernetes Engine (GKE), and how does it facilitate container orchestration?

- Google Kubernetes Engine (GKE) is a managed Kubernetes service provided by Google Cloud Platform (GCP). It facilitates container orchestration by abstracting away the underlying infrastructure complexities and providing a managed environment for deploying, managing, and scaling containerized applications using Kubernetes.

- Key features of GKE include:

    - Automated Kubernetes cluster management: GKE automates the deployment, scaling, and maintenance of Kubernetes clusters, allowing users to focus on deploying and managing applications rather than managing infrastructure.

    - Native Kubernetes integration: GKE offers seamless integration with Kubernetes, providing users with access to Kubernetes APIs, commands, and tools for managing containerized workloads.

    - Scalability and flexibility: GKE enables users to scale their applications horizontally by adding or removing container instances dynamically based on workload demand. It also supports hybrid and multi-cloud deployments, allowing users to run Kubernetes clusters across on-premises and cloud environments.

    - High availability and reliability: GKE ensures high availability and reliability of applications by automatically managing cluster upgrades, node failovers, and load balancing across multiple zones within a region.

    - Security and compliance: GKE provides built-in security features such as network policies, identity and access management (IAM) integration, and encrypted communication between services to ensure the security and compliance of containerized workloads.

- Overall, Google Kubernetes Engine (GKE) simplifies container orchestration by providing a managed Kubernetes platform that abstracts away infrastructure complexities, enabling users to deploy, manage, and scale containerized applications efficiently and reliably.

## Describe the purpose of Google Cloud Pub/Sub and provide an example of its use case.

- Google Cloud Pub/Sub is a fully managed messaging service provided by Google Cloud Platform (GCP). Its primary purpose is to facilitate scalable and reliable real-time messaging between applications and services.

- Example use case:

    - Suppose you have a system where multiple microservices need to communicate with each other asynchronously. You can use Google Cloud Pub/Sub to decouple these services and enable them to exchange messages in a reliable and scalable manner. Each microservice can publish messages to a specific topic, and other services can subscribe to these topics to receive and process messages as needed.

    - For instance, in an e-commerce application, when a customer places an order, the order service can publish an "order placed" message to a Pub/Sub topic. The inventory service can subscribe to this topic to receive notifications about new orders and update the available inventory accordingly. Similarly, the shipping service can subscribe to the same topic to initiate the shipping process for new orders. This decoupled architecture allows each service to operate independently and scale efficiently, ensuring reliable communication and system resilience.

## How do you set up a load balancer in Google Cloud Platform?

- Navigate to the Google Cloud Console and go to the "Network Services" section.

- Click on "Load balancing" to start the load balancer creation process.

- Choose the type of load balancer you want to create: HTTP(S) Load Balancing, TCP/SSL Proxy Load Balancing, or Network Load Balancing.

- Configure the load balancer settings, including:

    - Frontend configuration: Specify the IP address, port, protocol (HTTP, HTTPS, TCP, etc.), and SSL certificate (if applicable) for the load balancer's frontend.
    - Backend configuration: Define the backend services or instances that will receive traffic from the load balancer. You can specify a group of VM instances, managed instance groups, or other backends depending on the type of load balancer.
    - Health checks: Configure health checks to monitor the health of your backend instances and ensure they are responsive and available to handle incoming requests.
    - Load balancing algorithm: Choose the load balancing algorithm (e.g., round-robin, least connections) that determines how incoming requests are distributed among backend instances.

- Review and finalize your load balancer configuration.

- Click on "Create" to deploy the load balancer.

## What is Cloud Identity and Access Management (IAM) in GCP, and how is it used to manage permissions?

- Cloud Identity and Access Management (IAM) in Google Cloud Platform (GCP) is a service that allows you to manage access control and permissions for resources within your GCP projects.

- IAM enables you to:

    - Define who (identities) can do what (permissions) on specific resources.
    - Assign roles to identities, which determine the set of permissions they have.
    - Control access at the project, folder, and resource level.

- IAM is used to manage permissions by associating roles with identities. Roles are sets of permissions that grant access to specific actions on resources. Identities can be individual users, groups of users, or service accounts.

- Example:

    - You can use IAM to grant permissions to a group of developers to create and manage resources in a GCP project. You would create a custom role or assign predefined roles (such as the "Editor" role) to the group. This role would include permissions to create, update, and delete resources within the project. Other users or groups might be assigned roles with more limited permissions, such as the "Viewer" role, which allows read-only access to resources.

- By using IAM, you can ensure that only authorized users and services have access to your GCP resources, helping to enforce security and compliance requirements within your organization.

## How do you monitor and log activities in Google Cloud Platform?

- In Google Cloud Platform (GCP), you can monitor and log activities using Cloud Monitoring and Cloud Logging services.

    - Cloud Monitoring: Cloud Monitoring allows you to collect, view, and analyze metrics from various GCP services and resources in real-time. You can set up monitoring dashboards to visualize key performance indicators, create alerting policies to notify you of any abnormal behavior or incidents, and use advanced features like uptime checks and service monitoring to ensure the reliability and availability of your applications and services.

    - Cloud Logging: Cloud Logging enables you to capture, store, and analyze logs from GCP services and applications. You can centralize logs from multiple sources, including Compute Engine, Kubernetes Engine, App Engine, and Cloud Functions, and search, filter, and export logs using powerful query capabilities. Cloud Logging also integrates with other GCP services like Cloud Monitoring and Cloud Pub/Sub, allowing you to stream logs in real-time, trigger alerts based on log events, and perform advanced log analysis and troubleshooting.

- By using Cloud Monitoring and Cloud Logging together, you can gain insights into the performance, availability, and security of your GCP resources, and effectively monitor and troubleshoot issues in your environment.

### [<- Previous](./DR-intermediate.md) [Next ->](./GCP-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)