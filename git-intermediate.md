# Git - intermediate

### [README](./README.md)

## Describe the difference between a Git merge and a Git rebase. When would you choose one over the other?

- A Git merge integrates changes from one branch into another, preserving the commit history of both branches. A Git rebase rewrites commit history by moving or combining commits from one branch onto another, resulting in a linear history.

- Choose merge for collaborative work with multiple contributors, preserving original commit history. Choose rebase for a cleaner, linear history and when working on feature branches or before merging changes into a main branch.

## Explain the purpose of Git hooks and provide examples of how they can be used in a Git workflow.

- Git hooks are scripts that run automatically before or after specific Git events, such as committing, merging, or pushing. They allow customizing and automating actions in a Git workflow.

- Examples of Git hook usage:

    - Pre-commit hook to enforce code formatting or run linting checks before committing.
    - Post-commit hook to trigger automatic deployment or notification after a commit.
    - Pre-push hook to run unit tests or code validation before pushing changes to a remote repository.
    - Pre-receive hook on the server-side to enforce branch policies or reject invalid commits.

- Overall, Git hooks help enforce best practices, automate tasks, and maintain consistency in a Git workflow.

## What is Git rebase interactive? How can it be used to squash multiple commits into one?

- Git rebase interactive allows users to modify the commit history by interactively reordering, squashing, or editing commits during the rebase process. To squash multiple commits into one using interactive rebase:

    - Start an interactive rebase: 

    ```bash
    git rebase -i <commit>
    ```

    - Replace "pick" with "squash" or "s" for the commits you want to squash.
    - Save and close the editor.
    - Git will prompt you to provide a new commit message for the squashed commits.
    - Save the commit message and complete the rebase.

- This process combines the selected commits into one, preserving their changes in a single commit.

## Describe the Git flow branching model and its advantages in managing feature development and releases.

- Main Branches:

    - Master: Represents the stable production-ready code.
    - Develop: Integration branch for ongoing development.

- Supporting Branches:

    - Feature: Branches off from 'develop' for developing new features.
    - Release: Prepares for a new release, derived from 'develop' and merged back into both 'develop' and 'master'.
    - Hotfix: Created from 'master' to fix critical issues in production, merged back into both 'master' and 'develop'.

- Advantages of Git flow:

    - Isolation: Isolates features and bug fixes in separate branches, ensuring clean and manageable history.
    - Stability: 'Master' branch always contains stable, production-ready code.
    - Collaboration: Facilitates collaboration among team members by defining clear branching and merging strategies.
    - Versioning: Enables easy versioning and release management through dedicated release branches.

- Overall, the Git flow model provides a structured approach to Git workflow, promoting collaboration, stability, and efficient release management in software development projects.

## How do you resolve merge conflicts in Git? Provide steps you would take to resolve a conflict between two branches.

- Identify Conflicts:

    - Git indicates conflicted files with "<<<<<<< HEAD", "=======", and ">>>>>>>".
    - Open the conflicted file(s) in a text editor.

- Resolve Conflicts:

    - Manually edit the conflicted sections to resolve differences between the conflicting branches.
    - Remove conflict markers (<<<<<<<, =======, >>>>>>>) and ensure the file reflects the desired changes.

- Add Changes:

    - After resolving conflicts, stage the modified files using git add <file>.

- Complete Merge:

    - Commit the resolved changes with git commit -m "Merge conflict resolved".
    - Alternatively, if using a merge tool, follow the prompts to resolve conflicts and commit changes.

- Finish Merge:

    - If merging from a feature branch to the main branch, complete the merge with git merge <branch> or git merge --continue.

- Following these steps ensures conflicts are resolved and the merge is completed successfully, allowing for a smooth continuation of development.

## Explain the purpose of Git submodules and how they are used to manage dependencies in a Git repository.

- Git submodules are used to manage external dependencies within a Git repository. The purpose of Git submodules is to include external repositories as subdirectories within a parent repository.

- Key points about Git submodules:

    - Purpose: Manage external dependencies within a Git repository.
    - Usage: Add external repositories as submodules to a parent repository.
    - Integration: Submodules allow referencing specific commits or branches of external repositories.
    - Updates: Submodules can be updated to fetch changes from the external repositories.
    - Isolation: Each submodule maintains its own version history and can be managed independently.

- In summary, Git submodules facilitate the management of dependencies by integrating external repositories into a parent repository, providing a structured approach to handling project dependencies.

## Describe the Git cherry-pick command and provide a scenario where you would use it.

- The Git cherry-pick command is used to apply a specific commit from one branch to another. It allows developers to select and incorporate individual commits into their current branch.

- Scenario:
    
    - Suppose there are two branches in a Git repository: 'develop' and 'feature'. A critical bug fix has been committed to the 'develop' branch, but it is necessary to apply this fix to the 'feature' branch as well without merging the entire branch.

- To achieve this, you would:

    - Identify the commit containing the bug fix in the 'develop' branch.
    - Switch to the 'feature' branch using git checkout feature.
    - Cherry-pick the commit from the 'develop' branch using git cherry-pick <commit-hash>.

- This scenario demonstrates the use of cherry-pick to selectively apply commits across branches, enabling targeted bug fixes or feature enhancements without merging entire branches.

## How do you rewrite Git commit history using interactive rebase? What precautions should be taken when rewriting history?

- To rewrite Git commit history using interactive rebase:

    - Start Interactive Rebase: Execute git rebase -i <commit> where <commit> is the commit hash or branch name from where you want to rewrite history.

    - Edit Commits: In the interactive rebase editor, choose the action for each commit: pick, reword, edit, squash, fixup, or drop.

    - Execute Rebase: Save and close the editor. Git will apply the chosen actions and rewrite the commit history accordingly.

- Precautions when rewriting history:

    - Backup: Always create a backup or branch before performing an interactive rebase to avoid losing important changes.

    - Impact on Collaborators: Rewriting history can cause confusion for collaborators. Coordinate with team members before performing extensive history changes.

    - Avoid on Shared Branches: Avoid rewriting history on shared branches like 'master' to prevent conflicts and confusion for other team members.

    - Careful with Public Repositories: Exercise caution when rewriting history in public repositories as it can disrupt others who have cloned the repository.

    - Consider Branch Policies: Follow established branch policies and guidelines when rewriting history to maintain consistency within the project.

- By following these precautions, developers can safely use interactive rebase to manage and organize Git commit history effectively.

## Explain the difference between Git fetch and Git pull commands. When would you use each?

- Git fetch retrieves changes from a remote repository and stores them in the local repository, updating remote branch references without merging them into the current branch. It allows you to inspect the changes before integrating them into your local branch.

- Git pull, on the other hand, fetches changes from a remote repository and merges them into the current branch. It combines the 'git fetch' and 'git merge' commands into a single step, automatically integrating changes from the remote branch into the current local branch.

- Use Git fetch when you want to:

    - Retrieve changes from a remote repository without merging them into the current branch immediately.
    - Inspect changes before integrating them into your local branch.
    - Update remote branch references in the local repository.

- Use Git pull when you want to:

    - Retrieve changes from a remote repository and immediately merge them into the current branch.
    - Update the local branch with changes from the remote branch in a single step.
  - Quickly synchronize your local branch with the remote repository's state.

## Describe the Git bisect command and how it can be used to identify the commit that introduced a bug in the codebase.

- The Git bisect command is used to identify the commit that introduced a bug in the codebase by performing a binary search through the commit history. Here's how it works:

    - Start Bisect: Begin the bisect session with git bisect start.

    - Mark Good and Bad Commits: Identify a known "good" commit where the bug was not present and mark it with git bisect good, and a known "bad" commit where the bug is present and mark it with git bisect bad.

    - Bisecting: Git will automatically select a commit between the marked good and bad commits for testing.

    - Test the Code: Compile, run tests, or manually check the code to determine if the bug is present in the selected commit.

    - Mark the Commit: Based on the test results, mark the selected commit as good or bad using git bisect good or git bisect bad.

    - Repeat: Git will continue selecting commits for testing based on the binary search until the bug-introducing commit is identified.

    - Identify the Culprit: Once the bisect process identifies the commit that introduced the bug, Git will output the hash of the culprit commit.

- This process allows developers to efficiently pinpoint the exact commit that introduced a bug, enabling targeted debugging and resolution efforts.

### [<- Previous](./git-basic.md) [Next ->](./HPC-basic.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)