# Kubernetes - intermediate

### [README](./README.md)

## Explain the role of Kubernetes' declarative approach in managing application deployment and scaling.

- Kubernetes' declarative approach allows users to specify the desired state of their application infrastructure using YAML or JSON configuration files. This approach defines what the system should look like rather than how to achieve that state. Kubernetes continuously reconciles the actual state with the desired state, ensuring that the infrastructure matches the declared configuration. It simplifies deployment and scaling by automating resource provisioning, scaling, and management based on the defined specifications, reducing manual intervention and potential errors.

## Describe the difference between a Kubernetes StatefulSet and a Deployment. When would you use each?

- A Kubernetes StatefulSet is used to manage stateful applications that require stable, unique network identifiers and stable storage. StatefulSets maintain a unique identity for each pod and ensure ordered deployment and scaling. They are suitable for applications like databases, where each instance must have a persistent identity and data.

- On the other hand, a Kubernetes Deployment is ideal for stateless applications that can scale horizontally and do not require persistent storage. Deployments manage stateless pods, ensuring that a specified number of replicas are running and handling updates and rollbacks efficiently.

- In summary, StatefulSets are used for stateful applications that require stable identities and storage, while Deployments are suited for stateless applications that can scale horizontally and do not require persistent storage.

## How do you implement rolling updates and rollbacks for a Kubernetes Deployment?

- To implement rolling updates and rollbacks for a Kubernetes Deployment, you can follow these steps:

    - Update Deployment: Use the kubectl apply command with the updated configuration file or YAML manifest containing the changes you want to apply to the Deployment.

    - Monitor Update Progress: Monitor the progress of the rolling update using the kubectl rollout status command. This command shows the status of the update, including the number of updated and available replicas.

    - Pause Rollout (Optional): If needed, you can pause the rollout process using the kubectl rollout pause command. This allows you to investigate any issues or make adjustments before continuing the update.

    - Resume Rollout (Optional): Once you're ready to proceed, you can resume the rollout process using the kubectl rollout resume command.

    - Rollback (If Necessary): If the update encounters issues or you need to revert to a previous version, you can perform a rollback using the kubectl rollout undo command. This command reverts the Deployment to the previous version, effectively rolling back the changes.

    - Monitor Rollback: Monitor the progress of the rollback using the kubectl rollout status command to ensure that the Deployment is successfully reverted to the previous state.

- By following these steps, you can safely and efficiently perform rolling updates and rollbacks for a Kubernetes Deployment, ensuring minimal downtime and maintaining application availability.

## What is a Kubernetes Ingress, and how does it enable routing external traffic to services within a Kubernetes cluster?

- A Kubernetes Ingress is an API object that manages external access to services within a Kubernetes cluster. It acts as a layer 7 (application layer) HTTP or HTTPS router and enables the routing of external traffic to services based on rules defined by the user.

- Key points about Kubernetes Ingress:

    - Routing Rules: Ingress allows you to define routing rules based on criteria such as hostnames, paths, or header values. These rules determine how incoming requests should be directed to different services within the cluster.

    - HTTP and HTTPS: Ingress supports both HTTP and HTTPS traffic, allowing you to secure communication with TLS certificates.

    - Load Balancing: Ingress controllers typically integrate with external load balancers or provide their own load balancing mechanisms to distribute incoming traffic across multiple backend services.

    - Path-Based Routing: Ingress can route traffic based on the URL path, directing requests to different services or backend versions based on specific paths.

    - Host-Based Routing: Ingress can also route traffic based on the hostname in the request, allowing you to host multiple applications or services on the same IP address but under different domain names.

    - TLS Termination: Ingress controllers can terminate TLS encryption, decrypting incoming HTTPS traffic and forwarding it to backend services over unencrypted connections if necessary.

- Overall, Kubernetes Ingress simplifies the management of external access to services within a Kubernetes cluster by providing a flexible and customizable routing layer.

## Explain the concept of Kubernetes Secrets and how they are used to manage sensitive information like passwords or API keys.

- Kubernetes Secrets are objects used to store sensitive information such as passwords, API keys, and tokens. They are designed to securely manage and distribute this sensitive data to applications running within Kubernetes clusters. Here's how they work and how they are used:

    - Secure Storage: Kubernetes Secrets store sensitive data securely within etcd, the key-value store used by Kubernetes. Secrets are encrypted at rest to protect them from unauthorized access.

    - Base64 Encoding: Although Kubernetes Secrets are stored in etcd as base64-encoded strings, they are decoded and provided as plain text to applications running within pods.

    - Immutable: Once created, Secrets are immutable. This means that their data cannot be modified after creation. If you need to update a Secret, you must create a new one with the updated data.

    - Usage in Pods: Applications running within pods can consume Secrets as environment variables or mounted files. Kubernetes automatically injects Secret data into the pod's environment variables or mounts it as files in a specified directory.

    - Access Control: Kubernetes provides role-based access control (RBAC) mechanisms to control who can create, view, or modify Secrets within a cluster. This helps ensure that only authorized users or services can access sensitive information.

    - Types of Secrets: Kubernetes supports different types of Secrets, including generic Secrets (arbitrary key-value pairs), Docker registry Secrets (for pulling private Docker images), TLS Secrets (for storing TLS certificates and private keys), and more.

- Overall, Kubernetes Secrets provide a secure and convenient way to manage sensitive information within Kubernetes clusters, ensuring that applications can access the necessary credentials without compromising security.

## Describe the Kubernetes PersistentVolume and PersistentVolumeClaim objects. How do they enable persistent storage for applications?

- PersistentVolume (PV):

    - A PersistentVolume is a storage resource provisioned by the cluster administrator. It represents a piece of networked storage in the cluster, such as storage from cloud providers, NFS, or block storage.
    - PVs have a lifecycle independent of any individual pod. They can be dynamically provisioned by storage classes or statically provisioned by the cluster administrator.
    - Each PV has a set of properties that define its capacity, access modes (e.g., ReadWriteOnce, ReadOnlyMany, ReadWriteMany), storage class, and reclaim policy (e.g., Retain, Delete).
    - Once created, PVs are available for consumption by PersistentVolumeClaims.

- PersistentVolumeClaim (PVC):

    - A PersistentVolumeClaim is a request for storage by a pod. It allows pods to request specific storage resources (such as size and access mode) without needing to know the details of the underlying storage.
    - PVCs are bound to PVs based on matching criteria defined in the claim, such as capacity, access mode, and storage class.
    - When a PVC is created, Kubernetes attempts to find a suitable PV that matches the claim's requirements. If a matching PV is found, the PVC is bound to it.
    - Once bound, the PVC can be mounted into pods as a volume, providing persistent storage for the application running within the pod.

- By using PVs and PVCs, Kubernetes decouples storage provisioning and management from pod lifecycle management. This enables applications to have access to persistent storage that survives pod restarts, rescheduling, and even cluster failures, ensuring data persistence and availability across the lifecycle of the application.

## How does Kubernetes handle service discovery within a cluster, and what role does DNS play in this process?

- Service Discovery:

    - Kubernetes abstracts the details of service discovery by assigning each service in the cluster a unique DNS name. This DNS name is based on the service's name and namespace.
    - When a service is created, Kubernetes automatically assigns a DNS record for that service, allowing other pods within the cluster to discover and communicate with it using its DNS name.

- DNS Resolution:

    - Pods running within the Kubernetes cluster are automatically configured to use the cluster's internal DNS resolver. This resolver is responsible for resolving DNS queries for service names to their corresponding IP addresses.
    - When a pod needs to communicate with another service in the cluster, it can simply use the DNS name of the service in its network requests.
    - The DNS resolver within the cluster ensures that DNS queries for service names are resolved to the correct IP addresses of the pods backing those services.

- Example:

    - Suppose there's a service named my-service in the namespace my-namespace.
    - Any pod within the same namespace can communicate with my-service using the DNS name my-service.my-namespace.svc.cluster.local.
    - Kubernetes automatically resolves this DNS name to the IP address of one of the pods backing the my-service service.

- By leveraging DNS-based service discovery, Kubernetes simplifies the process of communication between services within the cluster. It abstracts away the complexities of network configuration and allows services to be dynamically discovered and accessed using intuitive DNS names.

## Explain the concept of Kubernetes Pod affinity and anti-affinity. Provide a scenario where each would be useful.

- Pod Affinity:

    - Pod affinity specifies rules to schedule a new pod onto a node with existing pods that match certain labels.
    - It can be useful in scenarios where certain services or applications benefit from being co-located with other related services to improve performance or reduce latency.
    - For example, in a microservices architecture, you might have a set of pods representing frontend services that need to communicate frequently with pods representing backend services. Using pod affinity, you can ensure that frontend pods are scheduled on the same node or nodes as backend pods, minimizing network latency and enhancing communication efficiency.

- Pod Anti-affinity:

    - Pod anti-affinity specifies rules to prevent a new pod from being scheduled onto a node with existing pods that match certain labels.
    - It's useful for enhancing availability and fault tolerance by ensuring that pods of the same service or application are spread across different nodes to reduce the risk of a single point of failure.
    - For instance, in a stateful application such as a database cluster, you might want to ensure that pods representing database instances are not co-located on the same node to minimize the impact of node failures. With pod anti-affinity, you can distribute database pods across different nodes, thereby improving resilience to node failures and enhancing the overall availability of the application.

- In summary, pod affinity is used to schedule pods onto nodes with specific existing pods, while pod anti-affinity is used to prevent pods from being co-located with certain existing pods. These features help optimize resource utilization, enhance performance, and improve the resilience of applications running in Kubernetes clusters.

## Describe the role of Kubernetes Operators and how they extend Kubernetes' functionality to manage complex, stateful applications.

- Automated Operations: Operators automate various operational tasks such as deployment, scaling, configuration, upgrades, and backup/restore procedures for stateful applications. They encapsulate human operational knowledge into code, enabling consistent and reliable management of complex applications.

    - Custom Resource Definitions (CRDs): Operators typically use Custom Resource Definitions (CRDs) to define custom objects that represent the state and configuration of the application they manage. These custom resources allow users to interact with the Operator through Kubernetes' API, enabling declarative management of application instances.

    - Control Loop: Operators implement a control loop that continuously monitors the state of application instances and reconciles any discrepancies between the desired state (specified by users through CRDs) and the observed state of the application. This ensures that the application remains in the desired state at all times, even in the face of failures or changes in the environment.

    - Domain-specific Knowledge: Operators encapsulate domain-specific knowledge about the application they manage, allowing them to perform intelligent actions based on the application's requirements and constraints. This enables Operators to make informed decisions about how to manage the application effectively.

    - Lifecycle Management: Operators handle the entire lifecycle of the application, from initial deployment to scaling, upgrades, and eventual decommissioning. They manage the complexities associated with stateful applications, such as data persistence, clustering, and synchronization, ensuring that the application remains available, reliable, and scalable.

    - Community-driven: Kubernetes Operators are often developed and maintained by the community, leveraging best practices and collective expertise to provide robust and feature-rich management solutions for a wide range of applications and use cases.

- In summary, Kubernetes Operators extend Kubernetes' functionality by automating the management of complex, stateful applications through the use of custom controllers, CRDs, and domain-specific knowledge. They enable declarative management, intelligent decision-making, and lifecycle automation for applications running in Kubernetes clusters, leading to improved operational efficiency, reliability, and scalability.

## How do you monitor and collect metrics from a Kubernetes cluster? Name some popular tools used for Kubernetes monitoring and their key features.

- Prometheus:

    - Prometheus is a widely-used open-source monitoring and alerting system designed for collecting and querying time-series data.
    - It has native support for Kubernetes, offering integrations with Kubernetes APIs to discover and monitor cluster components such as nodes, pods, and services.
    - Prometheus uses a pull-based model, where it scrapes metrics from instrumented applications and services at regular intervals.
    - It provides powerful querying capabilities with PromQL (Prometheus Query Language) for analyzing and visualizing collected metrics.
    - Prometheus can be combined with Grafana for creating dashboards and visualizing monitoring data effectively.

- Grafana:

    - Grafana is an open-source analytics and visualization platform that complements Prometheus for creating rich, interactive dashboards.
    - It integrates seamlessly with Prometheus, allowing users to query Prometheus metrics and create custom dashboards to monitor Kubernetes clusters.
    - Grafana offers a wide range of visualization options, including graphs, tables, heatmaps, and histograms, making it easy to gain insights from monitoring data.
    - It supports alerting and notification features, enabling users to define alert rules based on Prometheus metrics and receive notifications via various channels.

- Kubernetes Dashboard:

    - Kubernetes Dashboard is a web-based user interface for managing and monitoring Kubernetes clusters.
    - It provides an overview of cluster resources, including nodes, pods, deployments, services, and persistent volumes.
    - Kubernetes Dashboard offers basic monitoring capabilities, such as resource utilization metrics (CPU, memory) and pod health status.
    - While Kubernetes Dashboard is not as feature-rich as dedicated monitoring tools like Prometheus and Grafana, it provides a convenient way to visualize cluster metrics and perform basic administrative tasks.

- Heapster (deprecated, replaced by Metrics Server):

    - Heapster was an open-source project for collecting cluster-wide metrics in Kubernetes.
    - It was part of the Kubernetes ecosystem and provided metrics such as - - CPU, memory, and network usage for pods and nodes.
    - Heapster aggregated metrics from various data sources, including cAdvisor, kubelet, and other monitoring agents running on nodes.
    - However, Heapster has been deprecated in favor of Metrics Server, which serves a similar purpose but with a more lightweight and efficient architecture.

- Metrics Server:

    - Metrics Server is a cluster-wide aggregator of resource usage metrics in Kubernetes.
    - It collects metrics such as CPU and memory usage for pods and nodes and exposes them through the Kubernetes API server.
    - Metrics Server is designed to be lightweight and scalable, making it suitable for large-scale Kubernetes deployments.
    - While Metrics Server provides basic resource usage metrics, it may be supplemented with additional monitoring solutions like Prometheus for more advanced monitoring and alerting capabilities.

- These tools, along with others like Elasticsearch, Fluentd, and Kibana (EFK stack), provide comprehensive monitoring solutions for Kubernetes clusters, enabling DevOps teams to gain insights into cluster performance, troubleshoot issues, and ensure the reliability of containerized applications.

### [<- Previous](./k8s-basic.md) [Next ->](./linux-basic.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)