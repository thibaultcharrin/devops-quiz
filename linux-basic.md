# Linux - basic

### [README](./README.md)

## What command would you use to list the contents of a directory in Linux?

```bash
ls
```

## How can you create a new directory in Linux using the command line?

```bash
mkdir
```

## What command would you use to copy a file from one directory to another in Linux?

```bash
cp
```

## How do you move a file from one directory to another in Linux?

```bash
mv
```

## How can you delete a file in Linux using the command line?

```bash
rm
```

## What command would you use to display the contents of a text file in Linux?

```bash
cat
```

## How do you create a new empty text file in Linux using the command line?

```bash 
touch
```

## What command is used to find files and directories based on their names in Linux?

```bash
find . 
```

## How do you navigate to the home directory of the current user in Linux?

```bash
cd
```

## What command would you use to display the current date and time in Linux?

```bash 
date
```

### [<- Previous](./k8s-intermediate.md) [Next ->](./linux-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)