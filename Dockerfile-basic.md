# Dockerfile - basic

### [README](./README.md)

## What is a Dockerfile, and what purpose does it serve in Docker?

- Blueprint to create docker image

## How do you start creating a Dockerfile? What is the first instruction typically used?

```Dockerfile
# one of ...
FROM image:tag
FROM registry/namespace/image:tag
FROM scratch
```

## Explain the difference between a Docker image and a Docker container in the context of a Dockerfile.

- A docker image is a consolidation of a filesystem in a registry
- A container is an instance of this filesystem

## What is the purpose of the "FROM" instruction in a Dockerfile?

- To choose a base image for the Docker image

## How do you add files from your local system into a Docker image using a Dockerfile?

```Dockerfile
COPY files ./
```

## What is the purpose of the "RUN" instruction in a Dockerfile?

- Execute commands to prepare the container image 

## How do you specify the port that a Docker container should listen on in a Dockerfile?

```Dockerfile
EXPOSE 8080
```

## What is the purpose of the "CMD" instruction in a Dockerfile?

- This will be the set of commands used at the start of a container

## Explain the significance of the "COPY" instruction in a Dockerfile and how it differs from the "ADD" instruction.

- ADD provides the possibility to copy files directly from a URL and also automatically extract compressed archives

## How do you build a Docker image from a Dockerfile using the Docker CLI?

```bash
docker build -t 'test:latest' .
```

### [<- Previous](./ansible-intermediate.md) [Next ->](./Dockerfile-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)