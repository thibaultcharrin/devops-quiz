# Python - basic

### [README](./README.md)

## What is Python, and why is it commonly used in DevOps?

- Python is a high-level programming language known for its simplicity, readability, and versatility. It is commonly used in DevOps for tasks such as automation, scripting, infrastructure management, and web development. Its extensive libraries and frameworks, along with its strong community support, make it an ideal choice for building tools and applications in the DevOps workflow.

## How do you install Python on different operating systems?

- Windows:

    - Download the Python installer from the official website.
    - Run the installer executable.
    - Check the option to add Python to PATH during installation.
    - Follow the installation wizard instructions.

- macOS:

    - macOS typically comes with Python pre-installed.
    - However, you can install an alternative version using Homebrew or by downloading the installer from the official website.
    - Use the terminal to run the installer or Homebrew command.

- Linux:

    - Many Linux distributions come with Python pre-installed.
    - If not, you can install it using the package manager specific to your distribution (e.g., apt for Debian/Ubuntu, yum for CentOS/RHEL, dnf for Fedora).
    - Use the package manager to install the "python" package.

Ensure to verify the installation by running python --version or python3 --version depending on the version you installed.

## Explain the difference between Python 2 and Python 3.

- Syntax:

    - Python 3 has a cleaner and more consistent syntax compared to Python 2.
    - Python 3 requires parentheses for print() function, whereas Python 2 allows printing without parentheses.

- Unicode Support:

    - Python 3 treats strings as Unicode by default, whereas Python 2 treats them as ASCII unless specified otherwise.
    - This change in Python 3 helps in better handling of text and characters from different languages.

- Division Operator:

    - In Python 2, the division operator / performs integer division if both operands are integers.
    - In Python 3, the division operator / always performs floating-point division, yielding a float result.

- range() vs. xrange():

    - Python 3's range() function behaves like Python 2's xrange(), generating values lazily.
    - Python 3 doesn't have xrange().

- print() Function:

    - In Python 2, print is a statement, whereas in Python 3, it's a function.
    - In Python 3, print() function provides more flexibility and consistency.

- input() vs. raw_input():

    - In Python 2, input() evaluates the input as Python code, while raw_input() reads input as a string.
    - In Python 3, input() behaves like Python 2's raw_input(), while raw_input() is removed.

- __str__() vs. __unicode__():

    - In Python 2, __str__() is used for byte strings, and __unicode__() for Unicode strings.
    - In Python 3, there's only __str__() for Unicode strings, as byte strings are no longer supported.

- Library Compatibility:

    - Some Python 2 libraries may not be compatible with Python 3, requiring modifications or replacements.
    - Python 3 has a more extensive standard library and ecosystem.

- Overall, Python 3 is the recommended version for new projects due to its improved features, better Unicode support, and continued community support, while Python 2 has reached its end-of-life and is no longer receiving updates or support.

## What is PIP, and how do you use it to manage Python packages?

- PIP (Python Package Installer) is the standard package manager for Python. It allows you to easily install, upgrade, and manage Python packages from the Python Package Index (PyPI) or other package repositories.

- Here's how you can use PIP to manage Python packages:

    ```bash
    # installation
    python -m ensurepip --upgrade

    # installing packages
    pip install package_name

    # specifying versions
    pip install package_name==version_number

    # updating packages
    pip install --upgrade package_name

    # listing installed packages
    pip list

    # uninstalling packages
    pip uninstall package_name

    # requirements files
    pip install -r requirements.txt

    # freezing requirements
    pip freeze > requirements.txt
    ```

Overall, PIP simplifies the process of managing Python packages, making it easier to work with external libraries and dependencies in your Python projects.

## How do you create and execute a Python script?

```python
# example.py
print("Hello, World!")
```

```bash
python example.py
```

## Describe the use of virtual environments in Python development.

- Isolation:

    - Virtual environments create isolated environments for each project. This means that any Python packages installed within a virtual environment are only available to that specific environment and won't interfere with other projects or the system-wide Python installation.

- Dependency Management:

    - Virtual environments allow you to specify and manage project-specific dependencies. You can install, upgrade, or remove Python packages within a virtual environment without affecting other projects.

- Version Compatibility:
    - Virtual environments help ensure version compatibility between different projects. Each project can have its own set of dependencies and specific versions of packages without conflicts.

- Environment Reproducibility:

    - Virtual environments enable you to reproduce the exact environment needed for your project. By sharing the project's requirements.txt file, others can recreate the same environment and work with the project seamlessly.

- Development and Testing:

    - Virtual environments are particularly useful during development and testing phases. They provide a clean environment to test new packages or configurations without impacting other projects or the system environment.

```bash
python -m venv venv
source venv/bin/activate
```

## What is the purpose of the "if name == 'main':" statement in Python scripts?

- Module Execution: It allows the script to be executed as a standalone program when run directly, but not when imported as a module in another script. This condition ensures that the code block under this statement is only executed when the script is run directly, and not when it's imported as a module in another Python script.

- Testing and Debugging: It provides a convenient way to include test code or debugging statements within the script. The code under this statement is typically used for testing purposes or for running specific tasks when the script is executed directly.

```python
def main():
    # Main program logic here
    print("This is the main function.")

if __name__ == '__main__':
    # Execute the main function if the script is run directly
    main()
```

- In this example, when the script is executed directly, the main() function will be called. However, if the script is imported as a module in another script, the main() function will not be automatically executed. This allows the script to be used both as a standalone program and as a module in other Python scripts.

## How do you handle exceptions in Python, and what is the purpose of try-except blocks?

- Try Block: The code that may raise an exception is placed inside the try block.

- Except Block: If an exception occurs within the try block, the interpreter jumps to the except block. Here, you can handle the exception gracefully by providing appropriate error handling logic.

- The purpose of the try-except block is to gracefully handle exceptions that may occur during the execution of code. It allows you to anticipate and catch specific exceptions, preventing the program from crashing unexpectedly.

- Here's a basic example:

    ```python
    try:
        # Code that may raise an exception
        result = 10 / 0  # ZeroDivisionError: division by zero
    except ZeroDivisionError:
        # Handling the specific exception
        print("Cannot divide by zero.")
    ```

- In this example, if the division operation inside the try block raises a ZeroDivisionError, the interpreter jumps to the except block and executes the code within it, printing the error message "Cannot divide by zero." You can have multiple except blocks to handle different types of exceptions or a single except block to catch all exceptions.

## Explain the concept of list comprehension in Python with an example.

- List comprehension is a concise way to create lists in Python by applying an expression to each element in an iterable. It combines the for loop and the creation of new elements into a single line. Here's an example:

    ```python
    # Traditional approach using for loop
    numbers = [1, 2, 3, 4, 5]
    squared_numbers = []
    for num in numbers:
        squared_numbers.append(num ** 2)

    # Using list comprehension
    squared_numbers = [num ** 2 for num in numbers]
    ```

- In this example, we have a list of numbers [1, 2, 3, 4, 5]. We want to create a new list squared_numbers containing the squares of each number in the original list.

- With list comprehension, [num ** 2 for num in numbers], we iterate over each element num in the numbers list, calculate its square (num ** 2), and add it to the new list squared_numbers. This achieves the same result as the traditional for loop approach but in a more concise and readable manner.

## How do you iterate over elements in a list using a for loop in Python?

```python
my_list = [1, 2, 3, 4, 5]

for element in my_list:
    print(element)
```

### [<- Previous](./linux-intermediate.md) [Next ->](./python-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)
