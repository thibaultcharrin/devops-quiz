# Git - basic

### [README](./README.md)

## What is Git, and how does it differ from other version control systems?

- Distributed

## How do you initialize a new Git repository in a directory?

```bash
git init
```

## Explain the difference between Git's "add", "commit", and "push" commands.

```bash
git add <file> # adds file to staging area
git commit -m "message" # indexes changes to commit history (accessible via 'git log')
git push    # sends changes to remote origin repo
```

## How do you create a new branch in Git, and how do you switch between branches?

```bash
git branch <branch> # creates new branch
git checkout <branch> # goes to newly created branch
git checkout -b <branch>    # creates and goes to branch in one command
```

## Describe the purpose of the Git "clone" command and how it is used to create a local copy of a remote repository.

- The Git "clone" command is used to create a local copy of a remote repository. Its purpose is to replicate the entire history and contents of a remote Git repository onto the user's local machine.

## What is a Git remote, and how do you add a remote repository to your local Git repository?

```bash
git remote add origin <url> # adds a remote origin upstream to the local repo
```

## How do you view the commit history of a Git repository?

```bash
git log
git log --oneline --all --graph
```

## Explain the purpose of Git's "diff" command and how it helps in viewing changes between files.

- Compares current changes within files against last commit 

## How do you revert a commit in Git, and what are the potential consequences of doing so?

- revert a commit in Git, you can use the "git revert" command followed by the commit hash of the commit you want to revert. Here's how to do it:

    - Identify the commit: First, identify the commit you want to revert by its commit hash. You can use "git log" to view the commit history and find the hash of the commit you wish to revert.

    - Revert the commit: Once you have the commit hash, use the "git revert" command followed by the commit hash to create a new commit that undoes the changes introduced by the specified commit.

    ```bash
    git revert <commit_hash>
    ```

    - Resolve conflicts (if any): If the revert operation creates conflicts with other changes in the repository, you'll need to resolve them manually. Git will prompt you to resolve conflicts during the revert process.

    - Commit the revert: After resolving any conflicts, commit the changes introduced by the revert operation.

- Potential consequences of reverting a commit in Git:

    - Loss of changes: Reverting a commit undoes the changes introduced by that commit, potentially resulting in the loss of work. Ensure that reverting the commit is the correct action and won't cause any unintended consequences.

    - History preservation: While reverting a commit creates a new commit that undoes the changes, it preserves the commit history of the repository. This ensures that the original changes are still recorded in the commit history, providing a clear audit trail of the repository's changes over time.

    - Merge conflicts: Reverting a commit can sometimes result in merge conflicts if the changes introduced by the reverted commit conflict with other changes in the repository. You'll need to resolve these conflicts manually before committing the revert.

    - Impact on collaborators: If the repository is shared with collaborators, reverting a commit may affect their work if they have based their changes on the reverted commit. Communicate any revert actions to collaborators to ensure they are aware of the changes and can adjust their work accordingly.

- Overall, reverting a commit in Git should be done carefully, considering the potential consequences and impact on the repository's history and collaborators. It's essential to understand the changes introduced by the commit being reverted and ensure that reverting it is the appropriate action for the situation.

## Describe the process of merging branches in Git and how conflicts are resolved during the merge process.

- The process of merging branches in Git involves combining the changes from one branch into another branch. Here's how it works:

    - Checkout the target branch: First, ensure you are on the branch where you want to merge changes. Use the "git checkout" command to switch to the target branch.

    ```bash
    git checkout target_branch
    ```

    - Initiate the merge: Then, initiate the merge by using the "git merge" command followed by the name of the branch you want to merge into the current branch.

    ```bash
    git merge source_branch
    ```

    - Resolve conflicts: If Git detects any conflicts between the changes in the source branch and the target branch, it will pause the merge process and indicate the conflicted files. You'll need to manually resolve these conflicts by editing the affected files to resolve the conflicting changes.

    - Mark conflicts as resolved: After resolving the conflicts in the affected files, mark them as resolved using the "git add" command.

    ```bash
    git add conflicted_file1 conflicted_file2 ...
    ```

    - Complete the merge: Once all conflicts are resolved and marked as resolved, you can complete the merge by committing the changes using the "git commit" command.

    ```bash
    git commit -m "Merge branch 'source_branch' into 'target_branch'"
    ```

    - Push changes (if necessary): If you're merging branches in a shared repository and want to share the changes with others, push the merged changes to the remote repository using the "git push" command.

    ```bash
    git push origin target_branch
    ```

### [<- Previous](./GCP-intermediate.md) [Next ->](./git-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)