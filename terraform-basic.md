# Terraform - basic

### [README](./README.md)

## What is Terraform, and how does it differ from other infrastructure-as-code tools?

- Terraform is an open-source infrastructure-as-code tool developed by HashiCorp. It enables users to define and provision infrastructure resources, such as virtual machines, networks, and storage, using declarative configuration files.

- Key features of Terraform include:

    - Declarative configuration: Terraform uses a declarative approach to define infrastructure as code, allowing users to specify the desired state of the infrastructure without needing to define the step-by-step process to achieve that state.

    - Provider-based architecture: Terraform follows a provider-based architecture, where providers are responsible for interacting with specific infrastructure platforms, such as AWS, Azure, Google Cloud Platform, or VMware. This allows users to provision and manage resources across multiple cloud and on-premises environments using a unified interface.

    - Immutable infrastructure: Terraform promotes the concept of immutable infrastructure, where infrastructure changes are made by creating entirely new resources rather than modifying existing ones. This ensures consistency and reliability by reducing the risk of configuration drift and unintended changes.

    - Dependency management: Terraform automatically manages dependencies between infrastructure resources, ensuring that resources are created, updated, or destroyed in the correct order to maintain consistency and avoid race conditions.

    - Compared to other infrastructure-as-code tools like Ansible, Puppet, or Chef, Terraform differs in several ways:

    - Scope: Terraform focuses primarily on infrastructure provisioning and management, while tools like Ansible, Puppet, and Chef also provide configuration management capabilities.
    State management: Terraform maintains a state file that tracks the current state of managed infrastructure resources. This allows Terraform to perform actions such as plan, apply, and destroy based on the desired state defined in configuration files.
    
    - Execution model: Terraform uses a plan-apply model, where users first generate an execution plan to preview changes and then apply the plan to make changes to infrastructure resources. This differs from tools like Ansible, which typically execute tasks directly without generating an explicit plan.
    
    - Provider ecosystem: Terraform has a rich ecosystem of providers that enable integration with a wide range of cloud and on-premises infrastructure platforms. This makes Terraform suitable for heterogeneous environments where resources are provisioned across multiple providers.

## How do you install Terraform on your local machine?

- Read DOC, Use adequate package manager

## Explain the purpose of a Terraform configuration file (usually named "main.tf").

  - The purpose of a Terraform configuration file (usually named "main.tf") is to define the desired state of infrastructure resources that Terraform will manage. This file serves as the main entry point for Terraform, where you declare the infrastructure components you want to create, update, or delete.

  - In the main.tf file, you specify:

      - Provider configuration: Define the cloud or infrastructure provider you want to use, such as AWS, Azure, Google Cloud Platform, or others.

      - Resource declarations: Declare the resources you want to provision, such as virtual machines, networks, storage buckets, databases, and more. Each resource declaration includes configuration parameters that define the characteristics of the resource, such as its type, name, region, size, and other properties.

      - Data sources: Define data sources that Terraform can query to obtain information about existing infrastructure resources. Data sources are used to import existing resources into Terraform configurations or to retrieve information needed for resource provisioning.

      - Variables and outputs: Optionally, you can define input variables to parameterize your configurations and outputs to expose information about provisioned resources.

  - Overall, the main.tf file serves as the foundation of Terraform configurations, providing a declarative way to define infrastructure-as-code and manage infrastructure resources in a repeatable and consistent manner.

## How do you initialize a Terraform project in a directory containing Terraform configuration files?

```bash
terraform init
```

## What is a Terraform provider, and why is it necessary in Terraform configurations?

- A Terraform provider is a plugin responsible for interacting with a specific infrastructure platform, such as a cloud provider (e.g., AWS, Azure, Google Cloud Platform), a container orchestration platform (e.g., Kubernetes, Docker), or an on-premises infrastructure provider (e.g., VMware, OpenStack).

- Providers allow Terraform to manage resources and infrastructure components in a declarative manner by abstracting away the underlying API calls and details specific to each platform. They provide a unified interface for provisioning, updating, and deleting resources across different environments.

- In Terraform configurations, providers are necessary because they define the target platform where resources will be provisioned. By specifying the desired provider in the configuration files, Terraform knows which API endpoints to communicate with and how to authenticate with the platform. This enables Terraform to orchestrate infrastructure changes consistently and reliably across various environments, regardless of the underlying infrastructure provider.

## How do you define and create resources using Terraform? Provide an example of defining a resource in Terraform.

- To define and create resources using Terraform, you use Terraform configuration files written in HashiCorp Configuration Language (HCL). Here's a concise explanation of the process along with an example:

    - Define resource configuration: In a Terraform configuration file (typically named main.tf), you define the configuration for the desired resources using the appropriate Terraform resource block syntax.

    - Run terraform init: Initialize the Terraform project in the directory containing your configuration files to download any necessary plugins and modules.

    - Run terraform plan: Generate an execution plan to preview the changes that Terraform will make to your infrastructure.

    - Run terraform apply: Apply the execution plan to create, update, or delete resources as specified in your Terraform configuration.

- Here's an example of defining an AWS S3 bucket resource in Terraform:

  ```hcl
  # main.tf

  # Define AWS provider
  provider "aws" {
    region = "us-west-2"
  }

  # Define S3 bucket resource
  resource "aws_s3_bucket" "example_bucket" {
    bucket = "example-bucket"
    acl    = "private"
  }
  ```

## Describe the process of applying Terraform configuration to provision infrastructure resources.

- To apply Terraform configuration to provision infrastructure resources, follow these steps:

    - Define Terraform Configuration: Write Terraform configuration files (typically named main.tf, variables.tf, and outputs.tf) in HashiCorp Configuration Language (HCL) to define the desired state of infrastructure resources, including providers, resources, variables, and outputs.

    - Initialize Terraform: Navigate to the directory containing your Terraform configuration files in a terminal or command prompt and run terraform init. This command initializes the Terraform project, downloading necessary plugins and modules specified in the configuration files.

    - Plan Execution: Run terraform plan to generate an execution plan. Terraform compares the desired state defined in the configuration files with the current state of infrastructure resources and generates an execution plan to show what actions Terraform will take to achieve the desired state.

    - Review Execution Plan: Review the output of terraform plan to ensure that the proposed changes align with your expectations. Verify that Terraform will create, update, or delete resources as intended.

    - Apply Configuration: Once satisfied with the execution plan, execute terraform apply to apply the changes. Terraform will prompt for confirmation before proceeding. Confirm by entering "yes".

    - Provision Resources: Terraform applies the execution plan and provisions infrastructure resources according to the specified configuration. It creates, updates, or deletes resources as necessary to achieve the desired state.

    - Finalize: After completion, Terraform outputs a summary of the changes made and any relevant information about the provisioned resources. Review the output to ensure that resources were provisioned successfully.

- By following this process, Terraform enables declarative infrastructure provisioning, allowing you to define infrastructure as code and manage resources efficiently and consistently.

## What is Terraform state, and why is it important in managing infrastructure changes?

- Terraform state is a crucial aspect of Terraform's operation, as it serves as a record of the current state of managed infrastructure resources. It contains information about the resources that Terraform manages, such as their IDs, attributes, and dependencies.

- The Terraform state is important in managing infrastructure changes for several reasons:

    - Tracking resource state: Terraform uses the state file to track the current state of managed resources. This allows Terraform to determine the differences between the desired state defined in configuration files and the actual state of resources in the infrastructure.

    - Ensuring idempotent operations: Terraform relies on the state to perform idempotent operations during apply. It compares the desired state with the current state and only makes changes necessary to bring the infrastructure into the desired state. This helps prevent unintended modifications and ensures consistency in infrastructure management.

    - Concurrency and collaboration: The state file enables concurrency and collaboration by allowing multiple users to work on the same Terraform configuration concurrently. Terraform manages state locking to prevent conflicts and ensure that only one user can apply changes to the state at a time.

    - Resource dependency management: Terraform uses the state to manage dependencies between resources. It determines the order in which resources are created, updated, or deleted based on their dependencies defined in the configuration files.

- Overall, Terraform state plays a vital role in managing infrastructure changes, ensuring consistency, reliability, and predictability in infrastructure provisioning and management processes. It serves as a single source of truth for Terraform-managed resources and enables safe and efficient infrastructure automation workflows.

## How do you destroy resources provisioned by Terraform after they are no longer needed?

```hcl
terraform destroy
```

## Explain the concept of Terraform modules and how they facilitate code reuse and organization in Terraform configurations.

- Terraform modules are self-contained units of Terraform configuration that encapsulate reusable infrastructure components. They enable code reuse and organization in Terraform configurations by allowing you to define, package, and share infrastructure components as modular units.

- Key aspects of Terraform modules include:

    - Encapsulation: Modules encapsulate infrastructure components, including resources, variables, and outputs, within a single directory structure. This encapsulation promotes modularity and reusability by isolating components and their configurations.

    - Parameterization: Modules can accept input variables to customize their behavior and configuration. This parameterization allows you to tailor module usage to specific requirements without modifying the module code.

    - Composition: Modules can be composed and nested within other modules or configurations, enabling hierarchical organization and composition of infrastructure components. This allows you to build complex infrastructure topologies by combining and reusing modular components.

    - Reuse and sharing: Modules can be shared and reused across projects and environments, both within organizations and in the broader Terraform community. This promotes code reuse, standardization, and collaboration by enabling teams to leverage existing modules rather than reinventing the wheel.

- Overall, Terraform modules facilitate code reuse and organization in Terraform configurations by promoting modularity, parameterization, composition, and sharing of infrastructure components. They enable you to build scalable, maintainable, and consistent infrastructure-as-code solutions by encapsulating reusable components and promoting best practices in configuration management.
    
### [<- Previous](./storage-intermediate.md) [Next ->](./terraform-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)