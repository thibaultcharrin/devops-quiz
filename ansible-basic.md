# Ansible - basic

### [README](./README.md)

## What is Ansible, and how does it facilitate automation in IT infrastructure management?

- Ansible is an open-source automation tool used for IT infrastructure management. It facilitates automation by allowing users to define and manage infrastructure as code using simple, human-readable YAML files called playbooks. Ansible connects to remote hosts via SSH or WinRM and executes tasks defined in playbooks, enabling configuration management, application deployment, and orchestration across a variety of systems, including servers, networking devices, and cloud services. Its agentless architecture and idempotent nature make Ansible efficient, scalable, and easy to use for automating repetitive tasks, ensuring consistency, and streamlining IT operations.

## How do you install Ansible on a Linux system?

```bash
sudo pip install ansible
```

## Explain the difference between Ansible's ad-hoc commands and playbooks.

- Ad-hoc commands in Ansible are one-off commands that are executed directly from the command line without the need for creating separate files. They are useful for performing quick tasks, such as checking the status of services or running simple commands across multiple hosts simultaneously.

- On the other hand, playbooks in Ansible are YAML files that define a series of tasks to be executed on remote hosts in a structured and repeatable manner. Playbooks allow for more complex automation workflows, including configuration management, application deployments, and infrastructure orchestration. They provide flexibility, reusability, and the ability to organize tasks into logical sequences, making them suitable for managing entire environments and infrastructure setups.

## What is an Ansible inventory file, and why is it important in Ansible automation?

- An Ansible inventory file is a simple text file that lists the hosts or servers managed by Ansible. It contains information such as hostnames, IP addresses, and groupings of hosts based on criteria like environment, role, or function. The inventory file serves as the source of truth for Ansible to understand which systems it should manage and how to access them.

- The inventory file is important in Ansible automation because it provides a central location to define and organize the infrastructure that Ansible will manage. It allows operators to easily target specific hosts or groups of hosts for automation tasks, such as configuration management, software deployments, or system updates. Additionally, the inventory file enables dynamic inventory management, allowing for the integration of external systems like cloud providers or container orchestration platforms into Ansible workflows. Overall, the inventory file plays a crucial role in making Ansible automation scalable, maintainable, and flexible across diverse environments.

## How do you define tasks in an Ansible playbook?

```yaml
- name: Install Apache web server
  apt:
    name: apache2
    state: present

- name: Start Apache service
  service:
    name: apache2
    state: started
```

## Describe the purpose of Ansible modules and provide examples of commonly used modules.

- Ansible modules are reusable units of code that perform specific tasks on managed hosts. They encapsulate the logic required to carry out actions such as installing packages, managing files, manipulating services, and more. Modules abstract away platform-specific details, enabling consistent automation across different operating systems and environments.

- Examples of commonly used Ansible modules include:

    - apt/yum: Used for package management on Debian/Ubuntu-based or Red Hat-based systems, respectively.
    - copy: Copies files from the local machine to remote hosts.
    - file: Manages file attributes such as permissions, ownership, and content on remote hosts.
    - service: Controls system services like starting, stopping, or restarting.
    - shell/command: Executes shell commands or commands in a given shell on remote hosts.
    - template: Renders Jinja2 templates to generate configuration files on remote hosts.
    - user: Manages user accounts, including creation, modification, and deletion.
    - git: Manages Git repositories, allowing tasks like cloning, pulling, or checking out branches.
    - docker_container/podman_container: Manages Docker or Podman containers, providing functionalities like starting, stopping, or creating containers.
    - wait_for: Waits for a particular condition to be met on remote hosts before proceeding with subsequent tasks, such as waiting for a port to be available or a service to be responsive.

## How do you execute an Ansible playbook against a group of hosts?

```bash
ansible-playbook -i inventory_file playbook.yml
```

## Explain the concept of idempotence in Ansible and why it is crucial in automation.

- Idempotence in Ansible refers to the property where running a task multiple times results in the same outcome as running it once. In other words, if the system is in the desired state, Ansible will not make any changes, ensuring that subsequent runs of the playbook do not cause unnecessary modifications.

- This concept is crucial in automation because it promotes consistency and reliability. By ensuring that tasks are idempotent, Ansible reduces the risk of unintended changes and helps maintain the desired state of the infrastructure. It allows operators to confidently execute playbooks multiple times without worrying about causing unexpected side effects or discrepancies in the system. Idempotence is fundamental to achieving predictable and efficient automation workflows in Ansible.

## Describe the Ansible role and its significance in organizing and reusing automation tasks.

- An Ansible role is a reusable unit of automation that encapsulates a set of tasks, handlers, variables, and templates to accomplish a specific objective, such as configuring a service or deploying an application. Roles promote modularity and organization by allowing users to group related tasks and configurations into a single, self-contained unit.

- The significance of Ansible roles lies in their ability to facilitate code reuse and maintainability. By defining common tasks and configurations within roles, users can avoid duplication of effort and ensure consistency across multiple projects or environments. Roles also enable easier collaboration and sharing within teams or the Ansible community, as they can be distributed as standalone packages and easily integrated into different playbooks and projects.

- Overall, Ansible roles help streamline automation workflows, improve code readability, and simplify the management of complex infrastructure configurations. They are a fundamental building block in Ansible automation, allowing users to organize, reuse, and scale their automation tasks effectively.

## How do you handle sensitive data such as passwords or API keys in Ansible playbooks?

```bash
ansible-vault encrypt my_secret.yml
ansible-vault decrypt my_secret.yml
```

### [Next ->](./ansible-intermediate.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)
