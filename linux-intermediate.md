# Linux - intermediate

### [README](./README.md)

## How can you find all files larger than a specific size in a directory and its subdirectories in Linux?

```bash
find . -type f -size +300M
```

## Explain the difference between the "grep", "awk", and "sed" commands in Linux and provide a scenario where each would be useful.

- grep (simplest) used for finding text patterns:

    ```bash    
    grep 'Linux' linux-basic.md
    ```
  
- sed (intermediate) can find and modify data:

    ```bash  
    sed -e 's/Copyright 2020/Copyright 2021/g' index.html > modified.html 
    ```
  
- awk (programming language) can process text, perform comparison and arithmetic operations on extracted text (see names.txt):
  
    ```awk
    awk '($3 == "Toyota") {print}' names.txt    
    awk '($5 < 1945) {print $2}' names.txt
    awk '{total += $5} END {print total/NR}' names.txt
    ```
    
## How do you compress a directory into a tarball file in Linux, and how do you extract its contents later?

```bash
tar -czvf destination.tar.gz folder
tar -xzvf file.tar.gz 
```

## What command would you use to search for a specific string within multiple files in a directory and its subdirectories?

```bash
grep -rni "text string" /path/to/directory
grep -rli "text string" /path/to/directory
find /path/to/directory -type f -exec grep -l "text string" {} \;
```

## Describe the purpose and usage of the "chmod" command in Linux. Provide an example of how you would change the permissions of a file.

- chmod command changes a set of scoped permissions to a given file:

    ```bash
    chmod +x file 
    chmod 755 folder
    chmod 644 file
    ```

## How can you view real-time updates of a log file in Linux without opening the entire file?

```bash
tail -f /var/log/logfile
```

## Explain the purpose of the "cron" daemon in Linux. How would you schedule a task to run at a specific time using cron?

- Schdule tasks

    ```bash
    0 0 * * 0 /root/backup.sh
    ```

## What is the significance of the PATH environment variable in Linux? How would you add a directory to the PATH?

- Indicates where binaries are located in order to execute them from any directory

    ```bash
    export PATH=new-path:$PATH
    ```

## How do you use the "find" command in Linux to search for files modified within a specific time range?

```bash
find /var/tmp -mtime +2 -a -mtime -8 -ls # older than 2 days but not older than 8
```

## Describe the differences between hard links and symbolic links in Linux. When would you use one over the other?

```bash
ln    # portable shortcut
ln -s # shortcut in same directory
```

### [<- Previous](./linux-basic.md) [Next ->](./python-basic.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)