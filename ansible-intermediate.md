# Ansible - intermediate

### [README](./README.md)

## Explain the difference between Ansible's push and pull modes of operation. When would you use each?

- In Ansible's push mode of operation, the control machine executes tasks directly on remote hosts by pushing configuration and commands to them over SSH. This mode is suitable for environments where the control machine has direct access to the managed hosts, making it convenient for ad-hoc tasks or quick changes across a limited number of hosts.

- In Ansible's pull mode of operation, managed hosts periodically check a central repository, such as a version control system, for updated configuration files or playbooks. They then apply these configurations locally, effectively "pulling" changes from the repository. This mode is ideal for managing large-scale deployments across multiple hosts or for environments where the control machine cannot directly access the managed hosts, such as in a distributed or cloud-based infrastructure.

- You would typically use push mode for on-demand tasks or small-scale operations, while pull mode is better suited for managing larger, distributed environments with centralized configuration management.

## Describe how Ansible uses YAML files for defining playbooks and inventory. Provide examples of YAML syntax commonly used in Ansible.

```yaml
---
- name: Example playbook
  hosts: web_servers
  tasks:
    - name: Ensure Apache is installed
      yum:
        name: httpd
        state: present
      become: yes

    - name: Ensure Apache service is running
      service:
        name: httpd
        state: started
```

```yaml
all:
  hosts:
    web_servers:
      hosts:
        192.168.1.10:
        192.168.1.11:
    db_servers:
      hosts:
        192.168.1.20:
  vars:
    ansible_user: myuser
    ansible_ssh_private_key_file: /path/to/private_key.pem
```

## How do you manage secrets and sensitive data in Ansible, and what tools or methods can you use for encryption and decryption?

```bash
ansible-vault encrypt my_secret.yml
ansible-vault decrypt my_secret.yml
```

## Explain the concept of Ansible roles and how they promote code reuse and maintainability in Ansible playbooks.

- Ansible roles are self-contained units of automation that encapsulate a set of related tasks, variables, templates, and files. They promote code reuse and maintainability in Ansible playbooks by allowing users to organize and structure their automation tasks into reusable components.

- Roles encourage modularization and organization of playbooks, enabling users to break down complex automation workflows into smaller, more manageable pieces. This modularity promotes code reuse, as roles can be easily shared and reused across different playbooks and projects. It also facilitates collaboration within teams or the Ansible community, as roles can be distributed as standalone packages and easily integrated into various automation workflows.

- Additionally, roles promote maintainability by providing a consistent and structured approach to managing automation tasks. By defining common tasks and configurations within roles, users can avoid duplication of effort and ensure consistency across multiple projects or environments. Roles also simplify troubleshooting and debugging, as they isolate functionality into distinct units that can be tested and debugged independently.

- Overall, Ansible roles are a fundamental building block in Ansible automation, enabling users to organize, reuse, and scale their automation tasks effectively while promoting code maintainability and collaboration.

## Describe how Ansible handles error handling and retries within playbooks. Provide examples of error handling techniques.

```yaml
- name: Attempt to restart service (but ignore errors)
  service:
    name: myservice
    state: restarted
  ignore_errors: yes
```

```yaml
- name: Attempt to install package
  apt:
    name: mypackage
    state: present
  register: result
  failed_when: "'install' not in result.msg"
```

```yaml
- name: Attempt to restart service with retries
  service:
    name: myservice
    state: restarted
  retries: 3
  delay: 10
```

## What is Ansible Galaxy, and how does it help in sharing and reusing Ansible content like roles and playbooks?

- Ansible Galaxy is a hub for finding, sharing, and reusing Ansible content such as roles, playbooks, and collections. It serves as a centralized repository where users can discover pre-built automation content contributed by the Ansible community.

- Ansible Galaxy simplifies the process of sharing and reusing Ansible content in the following ways:

    - Discoverability: Users can easily search for Ansible roles and collections based on keywords, categories, or tags, making it straightforward to find relevant automation content for specific use cases.

    - Community contributions: Ansible Galaxy encourages collaboration and contribution within the Ansible community. Users can publish their own roles and collections, share best practices, and contribute improvements to existing content.

    - Versioning and dependencies: Ansible Galaxy provides versioning support, allowing users to specify dependencies and ensure compatibility between different roles and collections. This simplifies the management of dependencies and helps maintain consistency in automation workflows.

    - Quality assurance: Ansible Galaxy includes features for rating, reviewing, and tagging content, enabling users to assess the quality and reliability of roles and collections before integrating them into their automation workflows.

- Overall, Ansible Galaxy serves as a valuable resource for DevOps engineers, providing a platform for discovering, sharing, and collaborating on automation content, ultimately accelerating the development and deployment of Ansible-based infrastructure automation.

## How can you test Ansible playbooks before applying them to production environments? Describe the testing process and any tools you might use.

- You can test Ansible playbooks before applying them to production environments using several approaches and tools:

    - Syntax checking: Use the ansible-playbook --syntax-check command to check the syntax of your playbook files for any errors or typos.

    - Dry-run mode: Execute playbooks with the --check flag to perform a dry-run, which simulates playbook execution without making any changes to the system. This allows you to preview the changes that would occur if the playbook were run for real.

    - Linting: Use Ansible Lint, a static analysis tool, to check your playbook files against best practices and style conventions. Ansible Lint helps identify potential issues and improve the readability and maintainability of your playbooks.

    - Unit testing: Write unit tests for Ansible roles and playbooks using testing frameworks like Molecule or Testinfra. These frameworks allow you to define test scenarios and assertions to verify the correctness of your automation logic.

    - Integration testing: Use tools like KitchenCI or molecule to perform integration tests on your Ansible roles and playbooks against test environments. These tools allow you to spin up test instances, apply your playbooks, and validate the resulting configurations.

    - Continuous integration (CI): Incorporate Ansible playbook testing into your CI/CD pipeline using CI services like Jenkins, GitLab CI/CD, or GitHub Actions. Automate the execution of test suites on code changes to ensure that playbooks meet quality standards before being deployed to production.

- By combining these testing approaches and tools, you can ensure the reliability, correctness, and consistency of your Ansible playbooks before applying them to production environments, thereby minimizing the risk of unexpected issues and disruptions.

## Explain the concept of Ansible facts and how they provide information about managed hosts to Ansible playbooks.

- Ansible facts are pieces of information about managed hosts that Ansible gathers during playbook execution. These facts include details such as the host's operating system, IP address, network interfaces, available memory, and installed software packages.

- Ansible gathers facts from managed hosts using modules called "Gathering Facts" modules, such as the setup module. These modules collect system information and store it as variables that can be accessed and used within playbooks.

- Facts provide valuable insights into the state and configuration of managed hosts, enabling playbook authors to make informed decisions and perform targeted actions based on the host's characteristics. For example, you can use facts to conditionally execute tasks, install software packages specific to the host's operating system, or configure network settings based on the host's IP address.

- Overall, Ansible facts enhance the flexibility and effectiveness of playbooks by providing access to real-time information about managed hosts, enabling dynamic and context-aware automation workflows.

## Describe Ansible's inventory plugins and dynamic inventory sources. Provide examples of when you might use dynamic inventories.

- Ansible's inventory plugins and dynamic inventory sources enable dynamic discovery and management of hosts for automation tasks. Inventory plugins allow Ansible to retrieve inventory information from various sources such as cloud providers, container orchestrators, databases, or external systems.

- Dynamic inventories are particularly useful in scenarios where the infrastructure is dynamic, ephemeral, or managed by external systems. For example:

    - Cloud environments: Dynamic inventory plugins can query cloud providers like AWS, Azure, or Google Cloud Platform to dynamically generate inventory based on instances, regions, tags, or other criteria. This allows Ansible to seamlessly adapt to changes in cloud infrastructure, such as scaling instances up or down.

    - Container orchestrators: Ansible can use dynamic inventory plugins to query container orchestrators like Kubernetes, Docker Swarm, or OpenShift to discover and manage containers as hosts. This enables automation of containerized environments without the need to maintain static inventory files.

    - Infrastructure-as-Code (IaC) tools: Ansible can integrate with Infrastructure-as-Code tools like Terraform or CloudFormation to dynamically generate inventory based on infrastructure definitions. This ensures consistency between infrastructure provisioning and configuration management processes.

    - External databases or CMDBs: Dynamic inventory plugins can query external databases or Configuration Management Databases (CMDBs) to retrieve host information based on specific criteria or metadata. This allows Ansible to integrate seamlessly with existing infrastructure management systems.

- Overall, dynamic inventories provide flexibility and scalability in managing diverse and dynamic infrastructure environments, enabling Ansible to adapt to changes in real-time and automate tasks effectively.

## How do you integrate Ansible with other tools and systems in a DevOps environment, such as Jenkins or AWS services? Provide examples of integration scenarios.

- Ansible can be integrated with other tools and systems in a DevOps environment through various mechanisms, including:

    - Jenkins integration: Ansible can be invoked from Jenkins pipelines or jobs using the Ansible plugin for Jenkins. This allows Jenkins to orchestrate Ansible playbooks as part of continuous integration and continuous delivery (CI/CD) workflows. For example, Jenkins can trigger Ansible playbooks to provision infrastructure, deploy applications, or configure environments based on code changes.

    - AWS integration: Ansible provides AWS modules that allow interaction with AWS services such as EC2, S3, IAM, and more. Integration scenarios include provisioning and managing AWS infrastructure, deploying applications to EC2 instances, automating backups to S3, or managing IAM users and permissions using Ansible playbooks.

    - Configuration management tools: Ansible can integrate with configuration management tools like Puppet, Chef, or SaltStack to complement their capabilities or facilitate migration. For example, Ansible can be used to manage configuration drift on Puppet-managed nodes or perform post-configuration tasks after Chef runs converge.

    - Monitoring and logging tools: Ansible playbooks can automate tasks related to monitoring and logging systems, such as configuring monitoring agents, creating dashboards, or setting up log collection pipelines. Integration scenarios include using Ansible to deploy Prometheus exporters, configure Grafana dashboards, or provision Elasticsearch clusters.

    - Container orchestration platforms: Ansible can integrate with container orchestration platforms like Kubernetes, Docker Swarm, or OpenShift to automate tasks related to containerized environments. Integration scenarios include deploying applications to Kubernetes clusters, scaling Docker services, or managing Kubernetes resources using Ansible playbooks.

- Overall, Ansible's flexibility and extensibility allow it to integrate seamlessly with a wide range of tools and systems in a DevOps environment, enabling end-to-end automation and orchestration across the entire software delivery lifecycle.

### [<- Previous](./ansible-basic.md) [Next ->](./Dockerfile-basic.md) 

## TOC

- [ansible-basic](./ansible-basic.md)
- [ansible-intermediate](./ansible-intermediate.md)
- [Dockerfile-basic](./Dockerfile-basic.md)
- [Dockerfile-intermediate](./Dockerfile-intermediate.md)
- [DR-basic](./DR-basic.md)
- [DR-intermediate](./DR-intermediate.md)
- [GCP-basic](./GCP-basic.md)
- [GCP-intermediate](./GCP-intermediate.md)
- [git-basic](./git-basic.md)
- [git-intermediate](./git-intermediate.md)
- [HPC-basic](./HPC-basic.md)
- [HPC-intermediate](./HPC-intermediate.md)
- [jenkins-basic](./jenkins-basic.md)
- [jenkins-intermediate](./jenkins-intermediate.md)
- [k8s-basic](./k8s-basic.md)
- [k8s-intermediate](./k8s-intermediate.md)
- [linux-basic](./linux-basic.md)
- [linux-intermediate](./linux-intermediate.md)
- [python-basic](./python-basic.md)
- [python-intermediate](./python-intermediate.md)
- [storage-basic](./storage-basic.md)
- [storage-intermediate](./storage-intermediate.md)
- [terraform-basic](./terraform-basic.md)
- [terraform-intermediate](./terraform-intermediate.md)